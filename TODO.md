- Script does not execute necessary commands when neither github or bitbucket are chosen.  Fix that. (fixed)

2014-08-22:

  - a json file to store the username of the user running script and their associated bitbucket and github usernames (if not already stored in json file).
    - This would probably require an additional module (json), so add requrirement and update the Readme.md.  
  - Create a requirements file for user reference.
  - Add the ability to create a playground environment (no bitbucket or github, no project).  Just create the dir and git and that is it.  This is useful for when you want to just play with a new module or something.

2016-09-25:

  - add a pythong script template to the script to put into the base directory
  - create the lib/python directories in the base project directory
  - Add option to specify a URL for a locally hosted source code repo.  (need to check for api libraries for different systems)
